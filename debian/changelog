django-recurrence (1.12.1-1) unstable; urgency=low

  * New upstream version 1.12.1
  * Refresh patches.
  * Use wildcards instead of hardcoded version strings when removing dist-info
    files.

 -- Michael Fladischer <fladi@debian.org>  Mon, 14 Oct 2024 19:50:31 +0000

django-recurrence (1.12-2) unstable; urgency=low

  * Add patch to remove development dependencies from pyproject.toml.
  * Remove python3-tz from manual dependencies.

 -- Michael Fladischer <fladi@debian.org>  Fri, 06 Sep 2024 19:31:33 +0000

django-recurrence (1.12-1) unstable; urgency=low

  [ Debian Janitor ]
  * Apply multi-arch hints.

  [ Michael Fladischer ]
  * Use github instead of pypi for new releases.
  * New upstream version 1.12
  * Refresh patches.
  * Build using pybuild-plugin-pyproject and dh-sequence-python3.
  * Bump Standards-Version to 4.7.0.
  * Update year in d/copyright.
  * Use babel.messages.frontend to compile translations.
  * Simplify d/rules by using execute_ targets instead of overrides.
  * Replace python3-setuptools and python3-setuptools-scm with
    python3-pdm-backend.
  * Clean up pdb build artefacts to enable two builds in a row.

 -- Michael Fladischer <fladi@debian.org>  Fri, 06 Sep 2024 19:16:06 +0000

django-recurrence (1.11.1-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Michael Fladischer ]
  * New upstream release.
  * Use uscan version 4.
  * Use pytest to run upstream tests.
  * Add python3-setuptools-scm to Build-Depends as upstream now uses it
    in setup.py.
  * Enable upstream testsuite for autopkgtests.
  * Remove unnecessary autopkgtest-pkg-python testsuite.
  * Bump Standards-Version to 4.6.0.1.
  * Update year in d/copyright.
  * Add d/upstream/metadata.

 -- Michael Fladischer <fladi@debian.org>  Sat, 29 Jan 2022 08:25:23 +0000

django-recurrence (1.10.3-1) unstable; urgency=low

  * New upstream release.
  * Bump debhelper version to 13.

 -- Michael Fladischer <fladi@debian.org>  Mon, 08 Jun 2020 19:22:35 +0200

django-recurrence (1.10.2-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Michael Fladischer ]
  * New upstream release.
  * Bump Standards-Version to 4.5.0.
  * Clean up .pytest_cache/CACHEDIR.TAG to allow two builds in a row.
  * Set Rules-Requires-Root: no.
  * Tell autodep8 to use the correct import name.

 -- Michael Fladischer <fladi@debian.org>  Sun, 15 Mar 2020 21:34:23 +0100

django-recurrence (1.10.0-2) unstable; urgency=medium

  * Team upload.
  * Set the minimal version for the python3-pytest-django B-D to 3.5.1.

 -- Andrey Rahmatullin <wrar@debian.org>  Wed, 07 Aug 2019 22:10:13 +0500

django-recurrence (1.10.0-1) unstable; urgency=low

  * New upstream release.
  * Bump debhelper compatibility and version to 12 and switch to
    debhelper-compat.
  * Bump Standards-Version to 4.4.0.
  * Remove Python2 support.

 -- Michael Fladischer <fladi@debian.org>  Wed, 17 Jul 2019 15:24:00 +0200

django-recurrence (1.8.2-1) unstable; urgency=low

  * New upstream release.
  * Clean up files in .pytest_cache/ to allow two builds in a row.

 -- Michael Fladischer <fladi@debian.org>  Fri, 30 Nov 2018 11:49:06 +0100

django-recurrence (1.7.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs

  [ Michael Fladischer ]
  * New upstream release.
  * Clean up .pytest_cache/v/cache/nodeids to allow two builds in a row.
  * Bump Standards-Version to 4.2.1.

 -- Michael Fladischer <fladi@debian.org>  Sat, 20 Oct 2018 08:52:19 +0200

django-recurrence (1.6.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

  [ Michael Fladischer ]
  * New upstream release.
  * Bump debhelper compatibility and version to 11.
  * Bump Standards-Version to 4.1.4.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Enable autopkgtest-pkg-python testsuite.
  * Build documentation in override_dh_sphinxdoc.

 -- Michael Fladischer <fladi@debian.org>  Thu, 21 Jun 2018 17:39:22 +0200

django-recurrence (1.5.0-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches after git-dpm to gbp pq conversion
  * Refresh patches for 1.5.0.
  * Bump Standards-Version to 4.1.1.
  * Squash and extend patches to fix Language properties in .po files.
  * Use https:// for uscan URL.

 -- Michael Fladischer <fladi@debian.org>  Tue, 31 Oct 2017 09:08:38 +0100

django-recurrence (1.4.1-2) unstable; urgency=low

  * Add patch to fix values for Language field in translations (Closes:
    #865939).
  * Clean up django_recurrence.egg-info/requires.txt.
  * Bump Standards-Version to 4.0.0.

 -- Michael Fladischer <fladi@debian.org>  Thu, 13 Jul 2017 14:04:57 +0200

django-recurrence (1.4.1-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Wed, 11 Jan 2017 11:34:10 +0100

django-recurrence (1.4.0-1) unstable; urgency=low

  * New upstream release.
  * Cherry-pick patch for broken django.po file from upstream.
  * Use https:// for copyright-format 1.0 URL.

 -- Michael Fladischer <fladi@debian.org>  Wed, 19 Oct 2016 14:02:38 +0200

django-recurrence (1.3.0-2) unstable; urgency=medium

  * Team upload.
  * Added upstream fix for Django 1.10 (Closes: #828655):
    - 0002-Fixes-for-Django-1.10.patch

 -- Thomas Goirand <zigo@debian.org>  Fri, 05 Aug 2016 10:53:25 +0000

django-recurrence (1.3.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Fixed VCS URL

  [ Michael Fladischer ]
  * New upstream release.
  * Bump Standards-Version to 3.9.8.
  * Switch to python3-sphinx and drop versioned Build-Depends.
  * Add patch to fix exception handling during sphinx build with python3.
  * Replace python(3)-pytest with python(3)-pytest-django in Build-
    Depends.
  * Allow pybuild to run tests again.
  * Override dh_auto_test to set PYTHONPATH environment.
  * Rebuild djangojs domain translation from source.
  * Clean djangojs.mo files to allow two builds in a row.

 -- Michael Fladischer <fladi@debian.org>  Tue, 03 May 2016 20:33:00 +0200

django-recurrence (1.2.0-2) unstable; urgency=medium

  * Disable tests because they are not yet included in upstream tarball
    (Closes: #815392).
  * Initialize git-dpm.
  * Clean up django_recurrence.egg-info/SOURCES.txt to allow two builds in a
    row.
  * Rebuild translation catalogs from source using babel.
  * Bump Standards-Version to 3.9.7.
  * Use https in Vcs-Git field.

 -- Michael Fladischer <fladi@debian.org>  Tue, 01 Mar 2016 21:27:30 +0100

django-recurrence (1.2.0-1) unstable; urgency=low

  * Initial release (Closes: #795200).

 -- Michael Fladischer <fladi@debian.org>  Sun, 30 Aug 2015 12:31:01 +0200
